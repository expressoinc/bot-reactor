import express from 'express'
import bodyParser from 'body-parser'
import UserData from './UserData'
import Session from './Session'
import Emitter from '../../utils/Emitter'
import Connector from './Connector'

export default class Bot {

	constructor(configuration) {
		this._configuration = configuration

		this._connector = new Connector(this._configuration.authToken)

		this._messageEmitter = new Emitter()
		this._postbackEmitter = new Emitter()
		this._otherEmitter = new Emitter()
		this._eventEmitter = new Emitter()
	}

	setDefaultMessage(defaultMessage){
		this._defaultMessage = defaultMessage
	}

	middleware(){
		const app = express()
		app.use(bodyParser.json())

		app.post('/', (req, res, next) => {
			let respond = null

			if(req.body.event !== 'conversation_started'){
				res.sendStatus(200)
			}
			else {
				respond = (message) => {
					if(message == null){
						res.sendStatus(200)
						return
					}
					res.send({
						...this._defaultMessage,
						...message
					})
				}
			}

			this._handleMessaging(req.body, respond).catch(e => {
				if(this._ehandler != null) this._ehandler(e)
			})
		})

		return app
	}

	error(ehandler){
		this._ehandler = ehandler
	}

	init(...handlers){
		this._otherEmitter.on('init', handlers)
	}

	hookReceive(...handlers){
		this._otherEmitter.on('hook_receive', handlers)
	}

	hookSend(...handlers){
		this._otherEmitter.on('hook_send', handlers)
	}

	on(eventNames, ...handlers){
		this._eventEmitter.on(eventNames, handlers)
	}

	receiveWebhook(...handlers){
		this._otherEmitter.on('webhook', handlers)
	}

	receiveMessage(statusList, ...handlers){
		if(statusList instanceof Function){
			this._otherEmitter.on('listen', [statusList, ...handlers])
			return
		}
		this._messageEmitter.on(statusList, handlers)
	}

	receivePostback(buttonIdList, ...handlers){
		this._postbackEmitter.on(buttonIdList, handlers)
	}

	receiveSeen(...handlers){
		this._otherEmitter.on('seen', handlers)
	}

	receiveDelivered(...handlers){
		this._otherEmitter.on('delivered', handlers)
	}

	receiveFailed(...handlers){
		this._otherEmitter.on('failed', handlers)
	}

	receiveUnsubscribed(...handlers){
		this._otherEmitter.on('unsubscribed', handlers)
	}

	receiveSubscribed(...handlers){
		this._otherEmitter.on('subscribed', handlers)
	}

	receiveConversationStarted(...handlers){
		this._otherEmitter.on('conversation_started', handlers)
	}

	async emit(eventName, ...args){
		return await this._eventEmitter.emit(eventName, ...args)
	}

	async emitWithNewSession(userId, eventName, ...args){
		const userData = await UserData.loadByUserId(userId)
		const session = new Session(this, userData, null)
		return await this._eventEmitter.emit(eventName, session, ...args)
	}

	async sendMessageByUserId(userId, message){
		const id = await UserData.convertUserIdToViberId(userId)
		await this.sendMessageById(id, message)
	}

	async sendMessageById(id, message, trackingTag=null){
		const sentMessage = {
			receiver: id,
			...this._defaultMessage,
			...message
		}
		return this.sendMessage(sentMessage, trackingTag)
	}

	async sendMessage(message, trackingTag=null){
		const sentMessage = {
			event: 'message_sent',
			timestamp: Date.now(),
			message: message
		}

		if(await this._handleSentMessaging(sentMessage, trackingTag).catch(e=>{
			if(this._ehandler != null) this._ehandler(e)
		}) === true) return

		return this._connector.request('POST', 'pa/send_message', null, message)
	}

	async broadcastMessageByIds(ids, message){
		const sentMessage = {
			broadcast_list: ids,
			...this._defaultMessage,
			...message
		}
		return this._connector.request('POST', 'pa/broadcast_message', null, sentMessage)
	}

	async setWebhook(properties){
		return this._connector.request('POST', 'pa/set_webhook', null, properties)
	}

	async getAccountInfo(){
		return this._connector.request('GET', 'pa/get_account_info')
	}

	async getUserDetails(id){
		return this._connector.request('POST', 'pa/get_user_details', null, {id: id})
	}

	async getOnline(ids){
		return this._connector.request('POST', 'pa/get_online', null, {ids: ids})
	}

	async postById(id, message){
		return this._connector.request('POST', 'pa/post', null, {
			...this._defaultMessage,
			...{from: id},
			...message
		})
	}

	async _handleMessaging(messaging, respond){
		if(messaging.event === 'webhook'){
			await this._receiveWebhook(messaging)
			return
		}

		const session = await this._createSesson(messaging)
		if(session == null) return

		if(await this._otherEmitter.emit('hook_receive', session) === true) return

		if(session.isMessage()) await this._receiveMessage(session)
		else if(session.isDelivered()) await this._receiveDelivered(session)
		else if(session.isSeen()) await this._receiveSeen(session)
		else if(session.isFailed()) await this._receiveFailed(session)
		else if(session.isSubscribed()) await this._receiveSubscribed(session)
		else if(session.isUnsubscribed()) await this._receiveUnsubscribed(session)
		else if(session.isConversationStarted()) await this._receiveConversationStarted(session, respond)
	}

	async _handleSentMessaging(messaging, trackingTag=null){
		if(!this._otherEmitter.has('hook_send')) return false

		const session = await this._createSesson(messaging)
		if(session == null) return true
		return await this._otherEmitter.emit('hook_send', session, trackingTag) === true
	}

	async _createSesson(messaging){
		const {viberId, user} = this._getUserInfo(messaging)
		const userData = await this._createUserDataWithViberSender(viberId, user)
		if(userData == null) return null
		const session = new Session(this, userData, messaging)
		if(session.userData.isCreated) await this._otherEmitter.emit('init', session)
		return session
	}

	_getUserInfo(messaging){
		switch(true){
		case messaging.sender != null : return { viberId: messaging.sender.id, user: messaging.sender }
		case messaging.user != null : return { viberId: messaging.user.id, user: messaging.user }
		case messaging.user_id != null : return { viberId: messaging.user_id, user: null }
		case messaging.message != null && messaging.message.receiver != null :
			return { viberId: messaging.message.receiver, user: null }
		default: return { viberId: null, user: null }
		}
	}

	async _receiveWebhook(messaging){
		await this._otherEmitter.emit('webhook', messaging)
	}

	async _receiveMessage(session){
		await session.userData.activate()

		//Avoid url button bug
		if(session.messaging.message.text != null){
			if(session.messaging.message.text.match(/^(http|https|viber):\/\//) != null) return
		}

		//Handle postback message
		if(session.isPostback()){
			if(session.actionBody.payload == null) throw new Error('"payload" is required in the postback message.')
			const buttonId = session.actionBody.payload.split('/')[0]
			await this._postbackEmitter.emit(buttonId, session)
			return
		}

		//handle text message
		const status = session.userData.getStatus()
		if(status == null || !this._messageEmitter.has(status.split('/')[0])) {
			await this._otherEmitter.emit('listen', session)
			return
		}
		await this._messageEmitter.emit(status.split('/')[0], session)
	}

	async _receiveDelivered(session){
		await this._otherEmitter.emit('delivered', session)
	}

	async _receiveSeen(session){
		await session.userData.activate()
		await this._otherEmitter.emit('seen', session)
	}

	async _receiveFailed(session){
		await this._otherEmitter.emit('failed', session)
	}

	async _receiveSubscribed(session){
		await session.userData.activate()
		await this._otherEmitter.emit('subscribed', session)
	}

	async _receiveUnsubscribed(session){
		await session.userData.activate()
		await this._otherEmitter.emit('unsubscribed', session)
	}

	async _receiveConversationStarted(session, respond){
		await session.userData.activate()
		await this._otherEmitter.emit('conversation_started', session, respond)
	}

	async _createUserDataWithViberSender(viberId, sender){
		const userData = await UserData.loadByViberId(viberId)
		if(userData != null) return userData
		if(sender == null) return null
		return await UserData.createWithViber(sender, this._configuration.initialStatus)
	}

}