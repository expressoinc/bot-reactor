export default class Session {

	constructor(bot, userData, messaging){
		this.messaging = messaging
		this.userData = userData
		this._bot = bot
		this.actionBody = null
		if(messaging != null && messaging.event === 'message' && this.messaging.message.text != null){
			try{
				this.actionBody = JSON.parse(this.messaging.message.text)
			} catch(e) { /* do nothing */ }
		}
	}

	async send(message, trackingTag=null){
		return this._bot.sendMessageById(this.userData.getViber().id, message, trackingTag)
	}

	async wait(time){
		return new Promise((resolve)=>{
			setTimeout(()=>{
				resolve()
			}, time)
		})
	}

	async getUserDetails(){
		return this._bot.getUserDetails(this.userData.getViber().id)
	}

	isPostback(){
		return this.actionBody != null && this.actionBody.type === 'postback'
	}

	isMessage(){
		return this.messaging != null && this.messaging.event === 'message'
	}

	isDelivered(){
		return this.messaging != null && this.messaging.event === 'delivered'
	}

	isSeen(){
		return this.messaging != null && this.messaging.event === 'seen'
	}

	isFailed(){
		return this.messaging != null && this.messaging.event === 'failed'
	}

	isSubscribed(){
		return this.messaging != null && this.messaging.event === 'subscribed'
	}

	isUnsubscribed(){
		return this.messaging != null && this.messaging.event === 'unsubscribed'
	}

	isConversationStarted(){
		return this.messaging != null && this.messaging.event === 'conversation_started'
	}

}