import request from 'request-promise-native'
import url from 'url'

const BASE_URL = 'https://chatapi.viber.com'

export default class Connector {

	constructor(authToken){
		this._authToken = authToken
	}

	async request(method, endpoint, query=null, body=null){
		const response = await request({
			uri: url.resolve(BASE_URL, endpoint),
			method: method,
			headers: {
				'X-Viber-Auth-Token': this._authToken
			},
			qs: query,
			json: (body == null) ? true : body
		})

		if(response.status == null || response.status == 0) return response

		const e = new Error(JSON.stringify(response))
		e.error = response
		throw e
	}

}