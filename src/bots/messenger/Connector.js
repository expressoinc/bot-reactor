import request from 'request-promise-native'
import url from 'url'
import path from 'path'
import crypto from 'crypto'

const BASE_URL = 'https://graph.facebook.com'

export default class Connector {

	constructor(accessToken, appSecret=null, apiVersion=null){
		this._accessToken = accessToken
		this._apiVersion = apiVersion
		if(appSecret != null) {
			this._appsecretProof = crypto.createHmac('sha256', appSecret).update(accessToken).digest('hex')
		}
	}

	async request(method, endpoint, query, body){
		const qs = {}
		if(this._accessToken != null) qs['access_token'] = this._accessToken
		if(this._appsecretProof != null) qs['appsecret_proof'] = this._appsecretProof
		return await request({
			uri: url.resolve(BASE_URL, path.join(this._apiVersion || '', endpoint)),
			method: method,
			qs: (query == null) ? qs : Object.assign(qs, query),
			json: (body == null) ? true : body
		})
	}

	async requestBatch(items){
		const formData = {
			access_token: this._accessToken,
			batch: JSON.stringify(items.map(item=>{
				return {
					method: item.method,
					relative_url: item.endpoint,
					body: this._createBatchBody(item.body)
				}
			}))
		}
		return await request.post({
			url: url.resolve(BASE_URL, this._apiVersion || ''),
			formData: formData,
			json: true
		})
	}

	_createBatchBody(body){
		let texts = []
		for(let key in body){
			texts.push(`${key}=${encodeURIComponent(JSON.stringify(body[key]))}`)
		}
		return texts.join('&')
	}

}