export default class Session {

	constructor(bot, userData, messaging){
		this.messaging = messaging
		this.userData = userData
		this._bot = bot
	}

	async send(message, trackingTag=null){
		return this._bot.sendMessageById(this.userData.getMessenger().id, message, trackingTag)
	}

	async wait(time){
		return new Promise((resolve)=>{
			setTimeout(()=>{
				resolve()
			}, time)
		})
	}

	async read(){
		return this._bot.senderActionById(this.userData.getMessenger().id, 'mark_seen')
	}

	async typing(isOn){
		return this._bot.senderActionById(this.userData.getMessenger().id, isOn ? 'typing_on' : 'typing_off')
	}

	async getProfile(fields){
		return this._bot.getProfile(this.userData.getMessenger().id, fields)
	}

	isMessage(){
		return this.messaging.message != null && this.messaging.message.is_echo != true
	}

	isMessageEcho(){
		return this.messaging.message != null && this.messaging.message.is_echo == true
	}

	isDelivery(){
		return this.messaging.delivery != null
	}

	isRead(){
		return this.messaging.read != null
	}

	isPostback(){
		return this.messaging.postback != null
	}

	isAccountLinking(){
		return this.messaging.account_linking != null
	}

	isOptin(){
		return this.messaging.optin != null
	}

	isReferral(){
		return this.messaging.referral != null
	}

}