import models from '../../models'

export default class UserData {

	constructor(userEntry, isCreated){
		this.userEntry = userEntry
		this.isCreated = isCreated
	}

	static async loadByMessengerId(messengerId){
		let userEntry = await models.User.findOne({
			where: {
				accountStatus: 'active'
			},
			include: [
				{
					model: models.UserMessenger,
					as: 'messenger',
					where: {
						id: messengerId
					}
				},
				{ model: models.UserStatus, as: 'status' },
				{ model: models.UserContext, as: 'contexts' }
			]
		})
		if(userEntry == null) return null
		return new UserData(userEntry, false)
	}

	static async loadByUserId(userId){
		let userEntry = await models.User.findById(userId, {
			where: {
				accountStatus: 'active'
			},
			include: [
				{ model: models.UserMessenger, as: 'messenger' },
				{ model: models.UserStatus, as: 'status' },
				{ model: models.UserContext, as: 'contexts' }
			]
		})
		if(userEntry == null) return null
		return new UserData(userEntry, false)
	}

	static async createWithMessenger({id, firstName, lastName, profilePic, locale, timezone, gender=null}, initialStatus) {

		if(![id, firstName, lastName, profilePic, locale, timezone].every(v=>v!=null)){
			throw new Error('Invalid argument')
		}

		const transaction = await models.sequelize.transaction()
		const createdUserEntry = await models.User.create(null, {transaction: transaction})
		await models.UserStatus.create({
			userId: createdUserEntry.id,
			status: initialStatus
		}, {transaction: transaction})
		const result = await models.sequelize.query(`
			insert into
				userMessengers (userId, id, firstName, lastName, profilePic, locale, timezone, gender, createdAt, updatedAt)
				select :userId, :id, :firstName, :lastName, :profilePic, :locale, :timezone, :gender, NOW(), NOW()
				from
					dual
				where
					not exists (
						select
							*
						from
							users
							left outer join userMessengers on users.id = userMessengers.userId
						where
							userMessengers.id=:id
							and users.accountStatus='active'
					)
		`, {
			replacements: {
				userId: createdUserEntry.id,
				id: id,
				firstName: firstName,
				lastName: lastName,
				profilePic: profilePic,
				locale: locale,
				timezone: timezone,
				gender: gender
			},
			type: models.sequelize.QueryTypes.INSERT,
			transaction: transaction
		})

		const isCreated = result[1] !== 0
		if(!isCreated) await transaction.rollback()
		else await transaction.commit()

		const userEntry = await UserData.loadByMessengerId(id)
		userEntry.isCreated = isCreated
		return userEntry
	}

	static async convertUserIdToMessengerId(userId){
		const userEntry = await models.User.findById(userId, {
			include: [
				{ model: models.UserMessenger, as: 'messenger' }
			]
		})
		if(userEntry == null || userEntry.messenger == null) return null
		return userEntry.messenger.id
	}

	getId(){
		return this.userEntry.id
	}

	getMessenger(){
		return this.userEntry.messenger
	}

	async activate(){
		this.userEntry.activatedAt = new Date()
		await this.userEntry.save()
	}

	async reload(){
		await this.userEntry.reload()
	}

	getStatus(){
		if(this.userEntry.status == null) return null
		return this.userEntry.status.status
	}

	async setStatus(status) {
		await models.UserStatus.upsert({
			userId: this.userEntry.id,
			status: status
		})
		await this.userEntry.reload()
	}

	async deleteStatus(){
		if(this.userEntry.status == null) return
		await this.userEntry.status.destroy()
	}

	getContext(key){
		if(this.userEntry.contexts == null) return null
		const contexts = this.userEntry.contexts.filter((context) => {
			return context.key === key
		})
		return (contexts.length === 0) ? null : contexts[0].value
	}

	getAllContext(){
		if(this.userEntry.contexts == null) return null
		const contexts = {}
		this.userEntry.contexts.forEach(ctx => {
			contexts[ctx.key] = ctx.value
		})
		return contexts
	}

	async setContext(key, value){
		await models.UserContext.upsert({
			userId: this.userEntry.id,
			key: key,
			value: value
		})
		await this.userEntry.reload()
	}

	async deleteContext(key){
		await models.UserContext.destroy({
			where: {
				userId: this.userEntry.id,
				key: key
			}
		})
		await this.userEntry.reload()
	}

	async deleteAllContext(){
		await models.UserContext.destroy({
			where: {
				userId: this.userEntry.id
			}
		})
		await this.userEntry.reload()
	}
}