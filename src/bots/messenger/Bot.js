import express from 'express'
import bodyParser from 'body-parser'
import UserData from './UserData'
import Session from './Session'
import Connector from './Connector'
import Emitter from '../../utils/Emitter'

export default class Bot {

	constructor(configuration){
		this._configuration = configuration

		this._connector = new Connector(
			this._configuration.accessToken,
			this._configuration.appSecret,
			this._configuration.apiVersion
		)

		this._messageEmitter = new Emitter()
		this._messageEchoEmitter = new Emitter()
		this._postbackEmitter = new Emitter()
		this._optinEmitter = new Emitter()
		this._otherEmitter = new Emitter()
		this._eventEmitter = new Emitter()
	}

	middleware(){
		const app = express()
		app.use(bodyParser.json())

		app.get('/', (req, res, next) => {
			if (req.query['hub.verify_token'] === this._configuration.verifyToken) {
				res.send(req.query['hub.challenge'])
				return
			}
			res.send('Error, wrong validation token')
		})

		app.post('/', (req, res, next) => {
			res.sendStatus(200)
			this._receive(req.body).catch(e => {
				if(this._ehandler != null) this._ehandler(e)
			})
		})

		return app
	}

	error(ehandler){
		this._ehandler = ehandler
	}

	init(...handlers){
		this._otherEmitter.on('init', handlers)
	}

	hookReceive(...handlers){
		this._otherEmitter.on('hook_receive', handlers)
	}

	hookSend(...handlers){
		this._otherEmitter.on('hook_send', handlers)
	}

	on(eventNames, ...handlers){
		this._eventEmitter.on(eventNames, handlers)
	}

	receiveMessage(statusList, ...handlers){
		if(statusList instanceof Function){
			this._otherEmitter.on('listen', [statusList, ...handlers])
			return
		}
		this._messageEmitter.on(statusList, handlers)
	}

	receiveMessageEcho(statusList, ...handlers){
		this._messageEchoEmitter.on(statusList, handlers)
	}

	receivePostback(buttonIdList, ...handlers){
		this._postbackEmitter.on(buttonIdList, handlers)
	}

	receiveOptin(optinIdList, ...handlers){
		this._optinEmitter.on(optinIdList, handlers)
	}

	receiveAccountLinking(...handlers){
		this._otherEmitter.on('account', handlers)
	}

	receiveMessageRead(...handlers){
		this._otherEmitter.on('read', handlers)
	}

	receiveMessageDelivery(...handlers){
		this._otherEmitter.on('delivery', handlers)
	}

	receiveReferral(...handlers){
		this._otherEmitter.on('referral', handlers)
	}

	async emit(eventName, ...args){
		return await this._eventEmitter.emit(eventName, ...args)
	}

	async emitWithNewSession(userId, eventName, ...args){
		const userData = await UserData.loadByUserId(userId)
		const session = new Session(this, userData, null)
		return await this._eventEmitter.emit(eventName, session, ...args)
	}

	async sendMessageByUserId(userId, message){
		const id = await UserData.convertUserIdToMessengerId(userId)
		await this.sendMessageById(id, message)
	}

	async sendMessageById(id, message, trackingTag=null){
		const sentMessage = {
			recipient: {id: id},
			message: message
		}

		if(await this._handleSentMessaging(sentMessage, trackingTag).catch(e=>{
			if(this._ehandler != null) this._ehandler(e)
		}) === true) return

		return this.sendMessage(sentMessage)
	}

	async sendMessageByPhoneNumber(phoneNumber, message){
		return this.sendMessage({
			recipient: {phone_number: phoneNumber},
			message: message
		})
	}

	async senderActionById(id, action){
		return this.sendMessage({
			recipient:{id: id},
			sender_action: action
		})
	}

	async senderActionByPhoneNumber(phoneNumber, action){
		return this.sendMessage({
			recipient:{phone_number: phoneNumber},
			sender_action: action
		})
	}

	async sendMessage(message){
		return this._connector.request('POST', 'me/messages', null, message)
	}

	async sendMessages(messages){
		return this._connector.requestBatch(messages.map(message=>({
			method: 'POST',
			endpoint: 'me/messages',
			body: message
		})))
	}

	async getProfile(id, fields=['first_name', 'last_name', 'profile_pic', 'locale', 'timezone', 'gender', 'is_payment_enabled']){
		const query = {fields: fields.join(',')}
		const result = await this._connector.request('GET', id, query)
		if(!fields.every(field=>result[field] != null || field === 'gender')){
			throw new Error('Facebook profile was not correctly aquired')
		}
		return result
	}

	async getIdByAccountLinkingToken(accountLinkingToken){
		return this._connector.request('GET', 'me', {
			'fields': 'recipient',
			'account_linking_token': accountLinkingToken
		})
	}

	async setMessageCreatives(messages){
		return this._connector.request('POST', 'me/message_creatives', null, {messages: messages})
	}

	async sendBroadcastMessages(messageCreativeId, customLabelId=null, notificationType=null, tag=null){
		const messages = {message_creative_id: messageCreativeId}
		if(customLabelId != null) messages['custom_label_id'] = customLabelId
		if(notificationType != null) messages['notification_type'] = notificationType
		if(tag != null) messages['tag'] = tag
		return this._connector.request('POST', 'me/broadcast_messages', null, messages)
	}

	async getBroadcastMetrics(broadcastId){
		return this._connector.request('GET', `${broadcastId}/insights/messages_sent`)
	}

	async setBroadcastReachEstimations(customLabelId=null){
		const body = (customLabelId == null) ? null : {custom_label_id: customLabelId}
		return this._connector.request('POST', 'broadcast_reach_estimations', null, body)
	}

	async getBroadcastReachEstimations(reachEstimationId, fields=['name']){
		const query = (fields == null) ? null : {fields: fields.join(',')}
		return this._connector.request('GET', reachEstimationId, query)
	}

	async setCustomLabel(name){
		return this._connector.request('POST', 'me/custom_labels', null, {name: name})
	}

	async getCustomLabel(customLabelId, fields=['name']){
		const query = (fields == null) ? null : {fields: fields.join(',')}
		return this._connector.request('GET', customLabelId, query)
	}

	async getCustomLabels(userId=null, fields=['name']){
		const endpoint = `${(userId == null) ? 'me': userId}/custom_labels`
		const query = (fields == null) ? null : {fields: fields.join(',')}
		return this._connector.request('GET', endpoint, query)
	}

	async deleteCustomLabel(customLabelId){
		return this._connector.request('DELETE', customLabelId)
	}

	async setUserToCustomLabel(customLabelId, userId){
		return this._connector.request('POST', `${customLabelId}/label`, null, {user: userId})
	}

	async deleteUserFromCustomLabel(customLabelId, userId){
		return this._connector.request('DELETE', `${customLabelId}/label`, null, {user: userId})
	}

	async setMessengerProfile(properties){
		return this._connector.request('POST', 'me/messenger_profile', null, properties)
	}

	async getMessengerProfile(fields){
		return this._connector.request('GET', 'me/messenger_profile', {fields: fields.join(',')})
	}

	async deleteMessengerProfile(fields){
		return this._connector.request('DELETE', 'me/messenger_profile', null, { fields: fields })
	}

	async _receive(reqBody){
		if(!(reqBody.entry instanceof Array)) return
		if(reqBody.entry.length === 0) return

		for(let entry of reqBody.entry){
			if(!(entry.messaging instanceof Array)) continue
			for(let messaging of entry.messaging) {
				await this._handleMessaging(messaging)
			}
		}

	}

	async _handleMessaging(messaging){
		const session = await this._createSession(messaging)

		if(await this._otherEmitter.emit('hook_receive', session) === true) return

		if(session.isMessage()) await this._receiveMessage(session)
		else if(session.isMessageEcho()) await this._receiveMessageEcho(session)
		else if(session.isDelivery()) await this._receiveMessageDelivery(session)
		else if(session.isRead()) await this._receiveMessageRead(session)
		else if(session.isPostback()) await this._receiveMessagingPostback(session)
		else if(session.isAccountLinking()) await this._receiveMessagingAccountLinking(session)
		else if(session.isOptin()) await this._receiveMessagingOptin(session)
		else if(session.isReferral()) await this._receiveMessagingReferrals(session)
	}

	async _handleSentMessaging(messaging, trackingTag=null){
		if(!this._otherEmitter.has('hook_send')) return false
		const session = await this._createSession(messaging)
		return await this._otherEmitter.emit('hook_send', session, trackingTag) === true
	}

	async _createSession(messaging){
		const messengerId = this._getMessengerId(messaging)
		const userData = await this._createUserDataWithMessengerId(messengerId)
		const session = new Session(this, userData, messaging)
		if(session.userData.isCreated) await this._otherEmitter.emit('init', session)
		return session
	}

	async _receiveMessageEcho(session){
		const status = session.userData.getStatus()
		if(status == null) return
		await this._messageEchoEmitter.emit(status, session)
	}

	async _receiveMessage(session){
		await session.userData.activate()

		const status = session.userData.getStatus()
		if(status == null || !this._messageEmitter.has(status.split('/')[0])) {
			await this._otherEmitter.emit('listen', session)
			return
		}
		await this._messageEmitter.emit(status.split('/')[0], session)
	}

	async _receiveMessageDelivery(session){
		await this._otherEmitter.emit('delivery', session)
	}

	async _receiveMessageRead(session){
		await session.userData.activate()
		await this._otherEmitter.emit('read', session)
	}

	async _receiveMessagingPostback(session){
		await session.userData.activate()

		const buttonId = session.messaging.postback.payload.split('/')[0]
		await this._postbackEmitter.emit(buttonId, session)
	}

	async _receiveMessagingAccountLinking(session){
		await this._otherEmitter.emit('account', session)
	}

	async _receiveMessagingOptin(session){
		await session.userData.activate()
		const optinId = session.messaging.optin.ref.split('/')[0]
		await this._optinEmitter.emit(optinId, session)
	}

	async _receiveMessagingReferrals(session){
		await session.userData.activate()
		await this._otherEmitter.emit('referral', session)
	}

	_getMessengerId(messaging){
		if(messaging.message != null && messaging.message.is_echo === true) return messaging.recipient.id
		if(messaging.sender != null && messaging.sender.id != null) return messaging.sender.id
		if(messaging.recipient != null && messaging.recipient.id != null) return messaging.recipient.id
		return null
	}

	async _createUserDataWithMessengerId(messengerId){
		let userData = await UserData.loadByMessengerId(messengerId)

		if(userData == null){
			const profile = await this.getProfile(messengerId)
			const data = {
				id: messengerId,
				firstName: profile.first_name,
				lastName: profile.last_name,
				profilePic: profile.profile_pic,
				locale: profile.locale,
				timezone: profile.timezone,
				gender: profile.gender
			}
			userData = await UserData.createWithMessenger(data, this._configuration.initialStatus)
		}
		return userData
	}

}