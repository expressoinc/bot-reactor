import request from 'request-promise-native'
import url from 'url'
import path from 'path'

const VERSION = 'v1'
const BASE_URL = 'https://api.kik.com'

export default class Connector {

	constructor(apiKey, userName){
		this._apiKey = apiKey
		this._userName = userName
	}

	async request(method, endpoint, query, body){
		const qs = {}
		const auth = {
			user: this._userName,
			pass: this._apiKey
		}

		return await request({
			uri: url.resolve(BASE_URL, path.join(VERSION, endpoint)),
			method: method,
			auth: auth,
			qs: (query == null) ? qs : Object.assign(qs, query),
			json: (body == null) ? true : body
		})
	}

}