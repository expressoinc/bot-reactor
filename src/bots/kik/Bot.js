import express from 'express'
import bodyParser from 'body-parser'
import UserData from './UserData'
import Session from './Session'
import Emitter from '../../utils/Emitter'
import Connector from './Connector'

export default class Bot {

	constructor(configuration) {
		this._configuration = configuration

		this._connector = new Connector(this._configuration.apiKey, this._configuration.userName)

		this._messageEmitter = new Emitter()
		this._postbackEmitter = new Emitter()
		this._otherEmitter = new Emitter()
		this._eventEmitter = new Emitter()
	}

	middleware(){
		const app = express()
		app.use(bodyParser.json())

		app.post('/', (req, res, next) => {
			res.sendStatus(200)
			this._receive(req.body).catch(e => {
				if(this._ehandler != null) this._ehandler(e)
			})
		})

		return app
	}

	error(ehandler){
		this._ehandler = ehandler
	}

	init(...handlers){
		this._otherEmitter.on('init', handlers)
	}

	hookReceive(...handlers){
		this._otherEmitter.on('hook_receive', handlers)
	}

	hookSend(...handlers){
		this._otherEmitter.on('hook_send', handlers)
	}

	on(eventNames, ...handlers){
		this._eventEmitter.on(eventNames, handlers)
	}

	receiveWebhook(...handlers){
		this._otherEmitter.on('webhook', handlers)
	}

	receiveMessage(statusList, ...handlers){
		if(statusList instanceof Function){
			this._otherEmitter.on('listen', [statusList, ...handlers])
			return
		}
		this._messageEmitter.on(statusList, handlers)
	}

	receivePostback(buttonIdList, ...handlers){
		this._postbackEmitter.on(buttonIdList, handlers)
	}

	receiveStartChatting(...handlers){
		this._otherEmitter.on('start_chatting', handlers)
	}

	receiveFriendPicker(...handlers){
		this._otherEmitter.on('friend_picker', handlers)
	}

	receiveReferral(...handlers){
		this._otherEmitter.on('referral', handlers)
	}

	receiveScanData(...handlers){
		this._otherEmitter.on('scan_data', handlers)
	}

	receiveReadReceipt(...handlers){
		this._otherEmitter.on('read_receipt', handlers)
	}

	receiveDeliveryReceipt(...handlers){
		this._otherEmitter.on('delivery_receipt', handlers)
	}

	async emit(eventName, ...args){
		return await this._eventEmitter.emit(eventName, ...args)
	}

	async emitWithNewSession(userId, eventName, ...args){
		const userData = await UserData.loadByUserId(userId)
		const session = new Session(this, userData, null)
		return await this._eventEmitter.emit(eventName, session, ...args)
	}

	async _buildMessage(id, chatId, message){
		let sentMessage = {}
		delete message['trackingTag']

		if(message.payload != null){
			sentMessage = {
				...{to: id},
				...message.payload
			}
		} else {
			sentMessage = {
				...{to: id},
				...message
			}
		}

		if(chatId != null) sentMessage['chatId'] = chatId
		return sentMessage
	}

	async sendMessageById(id, messages, chatId=null, trackingTag=null){
		if(!(messages instanceof Array)) messages = [messages]

		const sentMessages = []
		for(let message of messages){
			const tag = message.trackingTag != null ? message.trackingTag : trackingTag
			const sentMessage = await this._buildMessage(id, chatId, message)
			if(await this._handleSentMessaging(sentMessage, tag).catch(e=>{
				if(this._ehandler != null) this._ehandler(e)
			}) === true) continue
			sentMessages.push(sentMessage)
		}

		return this.sendMessages(sentMessages)
	}

	async sendMessages(messages){
		const body = {messages: messages}
		return this._connector.request('POST', 'message', null, body)
	}

	async setWebhook(properties){
		return this._connector.request('POST', 'config', null, properties)
	}

	async getProfile(id){
		const result = await this._connector.request('GET', `user/${id}`, null)
		return result
	}

	async broadcastMessages(messages){
		for(let message of messages){
			if(await this._handleSentMessaging(message).catch(e=>{
				if(this._ehandler != null) this._ehandler(e)
			}) === true) continue
		}
		const body = {messages: messages}
		return this._connector.request('POST', 'broadcast', null, body)
	}

	async _receive(reqBody){
		if(!(reqBody.messages instanceof Array)) return
		if(reqBody.messages.length === 0) return

		for(let messaging of reqBody.messages){
			await this._handleMessaging(messaging)
		}
	}

	async _handleMessaging(messaging){
		const userData = await this._createUserDataWithKikInfo(messaging.from, messaging.chatId)
		if(userData == null) return
		const session = new Session(this, userData, messaging)

		if(session.userData.isCreated) await this._otherEmitter.emit('init', session)
		if(await this._otherEmitter.emit('hook_receive', session) === true) return

		if(session.isMessage()) await this._receiveMessage(session)
		if(session.isStartChatting()) await this._receiveStartChatting(session)
		if(session.isFriendPicker()) await this._receiveFriendPicker(session)
		if(session.isReferral()) await this._receiveReferral(session)
		if(session.isScanData()) await this._receiveScanData(session)
		if(session.isReadReceipt()) await this._receiveReadReceipt(session)
		if(session.isDeliveryReceipt()) await this._receiveDeliveryReceipt(session)
	}

	async _handleSentMessaging(messaging, trackingTag=null){
		if(!this._otherEmitter.has('hook_send')) return false

		const userData = await this._createUserDataWithKikInfo(messaging.to, messaging.chatId)
		if(userData == null) return true
		const session = new Session(this, userData, messaging)
		return await this._otherEmitter.emit('hook_send', session, trackingTag) === true
	}

	async _receiveMessage(session){
		await session.userData.activate()

		//Handle postback message
		if(session.isPostback()){
			if(session.messaging.metadata.payload == null) throw new Error('"payload" is required in the postback message.')
			const buttonId = session.messaging.metadata.payload.split('/')[0]
			await this._postbackEmitter.emit(buttonId, session)
			return
		}

		//handle text message
		const status = session.userData.getStatus()
		if(status == null || !this._messageEmitter.has(status.split('/')[0])) {
			await this._otherEmitter.emit('listen', session)
			return
		}
		await this._messageEmitter.emit(status.split('/')[0], session)
	}

	async _receiveStartChatting(session){
		await session.userData.activate()
		await this._otherEmitter.emit('start_chatting', session)
	}

	async _receiveFriendPicker(session){
		await session.userData.activate()
		await this._otherEmitter.emit('friend_picker', session)
	}

	async _receiveReferral(session){
		await session.userData.activate()
		await this._otherEmitter.emit('referral', session)
	}

	async _receiveScanData(session){
		await session.userData.activate()
		await this._otherEmitter.emit('scan_data', session)
	}

	async _receiveReadReceipt(session){
		await session.userData.activate()
		await this._otherEmitter.emit('read_receipt', session)
	}

	async _receiveDeliveryReceipt(session){
		await this._otherEmitter.emit('delivery_receipt', session)
	}

	async _createUserDataWithKikInfo(kikId, chatId){
		let userData = await UserData.loadByKikId(kikId)
		if(userData == null){
			if(chatId == null) return null
			const profile = await this.getProfile(kikId)
			const data = {
				id: kikId,
				chatId: chatId,
				firstName: profile.firstName,
				lastName: profile.lastName,
				profilePic: profile.profilePicUrl,
				profilePicLastModified: profile.profilePicLastModified,
				timezone: profile.timezone
			}
			userData = await UserData.createWithKik(data, this._configuration.initialStatus)
		}
		return userData
	}
}