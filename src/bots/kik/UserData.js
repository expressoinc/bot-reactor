import models from '../../models'

export default class UserData {

	constructor(userEntry, isCreated){
		this.userEntry = userEntry
		this.isCreated = isCreated
	}

	static async loadByKikId(kikId){
		let userEntry = await models.User.findOne({
			where: {
				accountStatus: 'active'
			},
			include: [
				{
					model: models.UserKik,
					as: 'kik',
					where: {
						id: kikId
					}
				},
				{ model: models.UserStatus, as: 'status' },
				{ model: models.UserContext, as: 'contexts' }
			]
		})
		if(userEntry == null) return null
		return new UserData(userEntry, false)
	}

	static async loadByUserId(userId){
		let userEntry = await models.User.findById(userId, {
			where: {
				accountStatus: 'active'
			},
			include: [
				{ model: models.UserKik, as: 'kik' },
				{ model: models.UserStatus, as: 'status' },
				{ model: models.UserContext, as: 'contexts' }
			]
		})
		if(userEntry == null) return null
		return new UserData(userEntry, false)
	}

	static async createWithKik({id, chatId, firstName=null, lastName=null, profilePic=null, profilePicLastModified=null, timezone=null}, initialStatus) {
		if (id == null || chatId == null) throw new Error('Invalid argument')

		const transaction = await models.sequelize.transaction()
		const createdUserEntry = await models.User.create(null, {transaction: transaction})
		await models.UserStatus.create({
			userId: createdUserEntry.id,
			status: initialStatus
		}, {transaction: transaction})
		const result = await models.sequelize.query(`
			insert into
				userKiks (userId, id, chatId, firstName, lastName, profilePic, profilePicLastModified, timezone, createdAt, updatedAt)
				select :userId, :id, :chatId, :firstName, :lastName, :profilePic, :profilePicLastModified, :timezone, NOW(), NOW()
				from
					dual
				where
					not exists (
						select
							*
						from
							users
							left outer join userKiks on users.id = userKiks.userId
						where
							userKiks.id=:id
							and users.accountStatus='active'
					)
		`, {
			replacements: {
				userId: createdUserEntry.id,
				id: id,
				chatId: chatId,
				firstName: firstName,
				lastName: lastName,
				profilePic: profilePic,
				profilePicLastModified: profilePicLastModified,
				timezone: timezone
			},
			type: models.sequelize.QueryTypes.INSERT,
			transaction: transaction
		})
		const isCreated = result[1] !== 0
		if(!isCreated) await transaction.rollback()
		else await transaction.commit()

		const userEntry = await UserData.loadByKikId(id)
		userEntry.isCreated = isCreated
		return userEntry
	}

	getId(){
		return this.userEntry.id
	}

	getChatId(){
		return this.userEntry.chatId
	}

	getKik(){
		return this.userEntry.kik
	}

	async activate(){
		this.userEntry.activatedAt = new Date()
		await this.userEntry.save()
	}

	async reload(){
		await this.userEntry.reload()
	}

	getStatus(){
		if(this.userEntry.status == null) return null
		return this.userEntry.status.status
	}

	async setStatus(status) {
		await models.UserStatus.upsert({
			userId: this.userEntry.id,
			status: status
		})
		await this.userEntry.reload()
	}

	async deleteStatus(){
		if(this.userEntry.status == null) return
		await this.userEntry.status.destroy()
	}

	getContext(key){
		if(this.userEntry.contexts == null) return null
		const contexts = this.userEntry.contexts.filter((context) => {
			return context.key === key
		})
		return (contexts.length === 0) ? null : contexts[0].value
	}

	getAllContext(){
		if(this.userEntry.contexts == null) return null
		const contexts = {}
		this.userEntry.contexts.forEach(ctx => {
			contexts[ctx.key] = ctx.value
		})
		return contexts
	}

	async setContext(key, value){
		await models.UserContext.upsert({
			userId: this.userEntry.id,
			key: key,
			value: value
		})
		await this.userEntry.reload()
	}

	async deleteContext(key){
		await models.UserContext.destroy({
			where: {
				userId: this.userEntry.id,
				key: key
			}
		})
		await this.userEntry.reload()
	}

	async deleteAllContext(){
		await models.UserContext.destroy({
			where: {
				userId: this.userEntry.id
			}
		})
		await this.userEntry.reload()
	}
}