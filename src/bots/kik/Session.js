export default class Session {

	constructor(bot, userData, messaging) {
		this.messaging = messaging
		this.userData = userData
		this._bot = bot
		this.storedMessages = []
	}

	async send(messages, trackingTag=null){
		this.storeMessage(messages, trackingTag)
		const sentMessages = this.storedMessages
		this.storedMessages = []
		const kikUser = this.userData.getKik()
		return this._bot.sendMessageById(kikUser.id, sentMessages, kikUser.chatId)
	}

	storeMessage(messages, trackingTag=null){
		if(messages instanceof Array){
			for(let message of messages){
				this.storedMessages = this.storedMessages.concat({payload: message, trackingTag: trackingTag})
			}
		} else {
			this.storedMessages.push({payload: messages, trackingTag: trackingTag})
		}
	}

	async wait(time){
		return new Promise((resolve)=>{
			setTimeout(()=>{
				resolve()
			}, time)
		})
	}

	isPostback(){
		return this.messaging.metadata != null && this.messaging.metadata.type === 'postback'
	}

	isMessage(){
		return ['text', 'picture', 'video', 'link', 'sticker'].includes(this.messaging.type)
	}

	isStartChatting(){
		return this.messaging.type === 'start-chatting'
	}

	isFriendPicker(){
		return this.messaging.type === 'friend-picker'
	}

	isReferral(){
		return this.messaging.metadata != null && this.messaging.metadata.type === 'referral'
	}

	isScanData(){
		return this.messaging.type === 'scan-data'
	}

	isReadReceipt(){
		return this.messaging.type === 'read-receipt'
	}

	isDeliveryReceipt(){
		return this.messaging.type === 'delivery-receipt'
	}
}