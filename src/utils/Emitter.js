export default class Emitter{

	handlersDict = []

	on(eventName, handler){
		const eventNames = (typeof eventName === 'string') ? [eventName] : ((eventName instanceof Array) ? eventName : null)
		const handlers = (handler instanceof Function) ? [handler] : ((handler instanceof Array) ? handler : null)

		if(eventNames === null || handlers === null) throw new Error('Invalid arguments')

		for(let eventName of eventNames){
			for(let handler of handlers){
				if(this.handlersDict[eventName] == null) this.handlersDict[eventName] = []
				this.handlersDict[eventName].push(handler)
			}
		}
	}

	has(eventName) {
		return this.handlersDict[eventName] != null
	}

	async emit(eventName, ...args){
		const handlers = this.handlersDict[eventName]
		if(handlers == null || handlers.length === 0) return false

		for(let handler of handlers){
			if(await handler(...args) === true) return true
		}

		return false
	}

}
