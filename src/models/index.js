import Sequelize from 'sequelize'
import config from 'config'

const sequelize = new Sequelize(
	config.database.name,
	config.database.username,
	config.database.password,
	config.database.options
)
const User = sequelize.import(__dirname + '/User')
const UserMessenger = sequelize.import(__dirname + '/UserMessenger')
const UserViber = sequelize.import(__dirname + '/UserViber')
const UserKik = sequelize.import(__dirname + '/UserKik')
const UserContext = sequelize.import(__dirname + '/UserContext')
const UserStatus = sequelize.import(__dirname + '/UserStatus')

User.hasOne(UserMessenger,{ as: 'messenger', foreignKey: 'userId'})
User.hasOne(UserViber,{ as: 'viber', foreignKey: 'userId'})
User.hasOne(UserKik,{ as: 'kik', foreignKey: 'userId' })
User.hasOne(UserStatus,{ as: 'status', foreignKey: 'userId'})
User.hasMany(UserContext,{ as: 'contexts', foreignKey: 'userId'})

exports.sequelize = sequelize
exports.User = User
exports.UserMessenger = UserMessenger
exports.UserViber = UserViber
exports.UserKik = UserKik
exports.UserContext = UserContext
exports.UserStatus = UserStatus
