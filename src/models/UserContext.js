import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('userContext', {
		userId: { type: Sequelize.INTEGER, allowNull: false, unique: 'uniqueUserIdKey' },
		key: { type: Sequelize.STRING, allowNull: false, unique: 'uniqueUserIdKey' },
		value: { type: Sequelize.STRING, allowNull: false }
	})
}