import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('userStatus', {
		userId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false},
		status: { type: Sequelize.STRING, allowNull: false }
	})
}