import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('userMessenger', {
		userId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false},
		id: { type: Sequelize.STRING, allowNull: false },
		firstName: {type: Sequelize.STRING, allowNull: false},
		lastName: {type: Sequelize.STRING, allowNull: false},
		profilePic: {type: Sequelize.STRING, allowNull: false},
		locale: {type: Sequelize.STRING, allowNull: false},
		timezone: {type: Sequelize.INTEGER, allowNull: false},
		gender: {type: Sequelize.STRING}
	}, {
		indexes: [
			{ fields: ['id'], name: 'indexId' }
		]
	})
}