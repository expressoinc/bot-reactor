import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('userKik', {
		userId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false},
		id: { type: Sequelize.STRING, allowNull: false },
		chatId: { type: Sequelize.STRING, allowNull: false },
		firstName: {type: Sequelize.STRING},
		lastName: {type: Sequelize.STRING},
		profilePic: {type: Sequelize.STRING},
		profilePicLastModified: {type: Sequelize.DATE},
		timezone: {type: Sequelize.STRING}
	}, {
		indexes: [
			{ fields: ['id'], name: 'indexId' }
		]
	})
}