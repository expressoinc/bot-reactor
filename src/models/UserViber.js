import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('userViber', {
		userId: { type: Sequelize.INTEGER, primaryKey: true, allowNull: false},
		id: { type: Sequelize.STRING, allowNull: false },
		name: {type: Sequelize.STRING},
		avatar: {type: Sequelize.STRING(1000)},
		country: {type: Sequelize.STRING},
		language: {type: Sequelize.STRING},
		primaryDeviceOs: {type: Sequelize.STRING},
		apiVersion: {type: Sequelize.INTEGER},
		viberVersion: {type: Sequelize.STRING},
		mcc: {type: Sequelize.INTEGER},
		mnc: {type: Sequelize.INTEGER}
	}, {
		indexes: [
			{ fields: ['id'], name: 'indexId' }
		]
	})
}