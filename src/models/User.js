import Sequelize from 'sequelize'

export default function(sequelize, DataTypes) {
	return sequelize.define('user', {
		accountStatus: { type: Sequelize.ENUM('active', 'canceled', 'banned'), allowNull: false, defaultValue: 'active' },
		canceledAt: { type: Sequelize.DATE },
		cancelReasonId: { type: Sequelize.INTEGER },
		cancelDescription: { type: Sequelize.STRING(1000) },
		bannedAt: { type: Sequelize.DATE },
		activatedAt: { type: Sequelize.DATE, defaultValue: DataTypes.NOW(), allowNull: false}
	})
}