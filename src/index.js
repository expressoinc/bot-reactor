import messenger from './bots/messenger'
import viber from './bots/viber'
import kik from './bots/kik/'
import models from './models'

export default {
	messenger,
	viber,
	kik,
	models
}