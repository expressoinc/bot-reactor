const botReactor = require('bot-reactor').default

const bot = new botReactor.viber.Bot({
	authToken: 'AUTH_TOKEN',
})

bot.sendMessageById('VIBER_USER_ID', {type: 'text', text: 'test'}).then(res=>{
	console.log(res)
}).catch(err=>{
	console.error(err)
})