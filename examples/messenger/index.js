const botReactor = require('bot-reactor').default

const bot = new botReactor.messenger.Bot({
	accessToken: 'ACCESS_TOKEN',
	verifyToken: 'VERIFY_TOKEN'
})

bot.sendMessageById('MESSENGER_USER_ID', {text: 'test'}).then(res=>{
	console.log(res)
}).catch(err=>{
	console.error(err)
})