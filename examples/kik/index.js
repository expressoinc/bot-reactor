const botReactor = require('bot-reactor').default

const bot = new botReactor.kik.Bot({
	apiKey: "API_KEY",
	userName: "USER_NAME",
})

bot.sendMessageById('KIK_USER_ID', {type: 'text', body: 'test'}).then(res=>{
	console.log(res)
}).catch(err=>{
	console.error(err)
})